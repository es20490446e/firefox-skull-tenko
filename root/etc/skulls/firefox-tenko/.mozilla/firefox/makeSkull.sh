#! /bin/bash
## Part of: firefox-skull-tenko
## License: Latest GNU Affero

set -e
here="$(realpath "$(dirname "${0}")")"


mainFunction () {
	rm --recursive --force "${here}/tenko-skull"
	copyElements
	rm --recursive --force "${here}/tenko-skull/extensions/staged"
	sanitizePrefs
}


checkPrefsExists () {
	if [[ ! -f "prefs.js" ]]; then
		echo "No prefs.js in the current dir" >&2
		exit 1
	fi
}


copyElements () {
	local element;
	local elements=(
		"extensions"
		"storage/default"
		"addons.json"
		"content-prefs.sqlite"
		"extension-preferences.json"
		"extension-settings.json"
		"extensions.json"
		"handlers.json"
		"places.sqlite"
		"prefs.js"
		"search.json.mozlz4"
		"sessionCheckpoints.json"
		"storage-sync-v2.sqlite"
		"storage-sync-v2.sqlite-shm"
		"storage-sync-v2.sqlite-wal"
		"xulstore.json"
	)

	mkdir --parents "${here}/tenko-skull/storage"

	for element in "${elements[@]}"; do
		cp --recursive "${here}/tenko/${element}" "${here}/tenko-skull/${element}"
	done
}


filesAlreadyRightIntoSkull () {
	find "tenko-skull" -maxdepth 1 -type f |
	cut --delimiter='/' --fields=2-
}


patterns () {
	local index=1
	local pattern
	local patterns=(
		"^//"
		", \"[[:alnum:]]+-[[:alnum:]]+-[[:alnum:]]+-[[:alnum:]]+-[[:alnum:]]+\""
		", \"{[[:alnum:]]+-[[:alnum:]]+-[[:alnum:]]+-[[:alnum:]]+-[[:alnum:]]+}\""
		", [[:digit:]][[:digit:]][[:digit:]][[:digit:]][[:digit:]][[:digit:]][[:digit:]]"
		".abi"
		"appversion"
		"devtools"
		"lang"
		"lastAppBuildId"
		"lastDir"
		"last-disqualified"
		"last_etag"
		"last.index"
		"media.gmp"
		"migrations"
		"print"
		"region"
		"services.sync"
		"sessionCount"
		"Schema"
		"skew"
		"sanitize.pending"
		"typeWas"
		"userAgent"
	)

	for pattern in "${patterns[@]}"; do
		printf '%s' "${pattern}"

		if [[ "${index}" -lt "${#patterns[@]}" ]]; then
			printf "|"
			index="$(("${index}"+1))"
		else
			printf "\n"
		fi
	done
}


sanitizePrefs () {
	cd "${here}/tenko-skull"
	checkPrefsExists

	local prefs; prefs="$(
		grep --extended-regex --invert-match "$(patterns)" "prefs.js" |
		grep "\S"
	)"

	echo "${prefs}"	> "prefs.js"
}


mainFunction
