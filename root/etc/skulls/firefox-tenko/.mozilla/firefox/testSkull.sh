#! /bin/bash
## Part of: firefox-skull-tenko
## License: Latest GNU Affero

set -e
here="$(realpath "$(dirname "${0}")")"
cd "${here}"

rm --recursive --force "tenko"
cp --recursive "tenko-skull" "tenko"
so firefox
